"""
One big for-loop to combine data for first 100 funders returned
from FundRef api with Altmetric data for the first 20 works that exist for that funder.

A work can be funded by more than one funder.

Two dataframes are created in this script. 
"frame" has <i>all</i> the data
headline_dataframe has just few key details for an overview.

Still working out best way to query dataframes and they are likely to change.

Credit to https://github.com/CrossRef/rest-api-doc/blob/master/rest_api.md
and 
http://www.altmetric.com/ for all the data.

HOW TO:

I use ipython and simply copy the script and use magic command %paste. 

This gives me the dataframes to play with in the console once the script has 
finished running.

"""


import requests
import time

import pandas as pd

#start with first 100 funders returned. Change rows=100 if want maximum 1000 per request

thousand_funders = requests.get('http://api.crossref.org/funders?rows=100').json()

total_results_available = thousand_funders['message']['total-results']


#create list of ids
funder_ids = [x['id'] for x in thousand_funders['message']['items']]

#uncomment to grab all 8000+ funders ids

"""offset = 1000
increment = 1000
while offset < total_results_available+1:
	query = {'rows':'1000',
	'offset': offset}
	thousand_funders = requests.get('http://api.crossref.org/funders', params=query).json()
	new_ids = [x['id'] for x in thousand_funders['message']['items']]
	funder_ids.extend(new_ids)
	offset = offset+increment
	"""
	



"""
for testing use these two funder_ids

funder_ids = ['501100003833',
'501100004835']

"""

#once have all the funder ids, get the works and their altmetric scores

index = 0
dict_of_funder_series = {}
for funder_id in funder_ids:
	print('index: ', index)
	funder_details = requests.get('http://api.crossref.org/funders/'+funder_id).json()
	print('Getting data for ', funder_details['message']['name'])
	funder_series = pd.Series(funder_details['message'])
	funders_work = 'http://api.crossref.org/funders/'+funder_id+'/works'
	work_data = requests.get(funders_work).json()
	if work_data['message']['total-results'] > 0:
		work_series = pd.Series(work_data['message']['items'])
		funder_series['works'] = work_series
		for work in funder_series['works']:
			
			doi = work['DOI']
			altmetric_request = requests.get('http://api.altmetric.com/v1/doi/'+doi)
			if altmetric_request.status_code == 404:
				print('no altmetric for', doi)
				work['altmetrics'] = None
				time.sleep(1.5)# so api isn't hammered
				
			elif altmetric_request.status_code == 200:
				print('altmetric data gathered for', doi)
				altmetrics = altmetric_request.json()
				#altmetric_series = pd.Series(altmetrics)
				work['altmetrics'] = altmetrics # add altmetrics to each work
				
				
				time.sleep(1.5)# so api isn't hammered			
			

	else:
		funder_series['works'] = None

	dict_of_funder_series[index]=funder_series
	index = index+1
	


frame = pd.DataFrame(dict_of_funder_series).T


"""
and then...
extract headline data
"""
index = 0
headline_data = {}
for works in frame.works:
	if works is not None:
		for work in works:
			if isinstance(work['altmetrics'], dict):
				headline_data[index] = {'doi':work['DOI'],
				'altmetrics':work['altmetrics']['score'],
				'funder':work['funder']
				}
				index = index+1

#I love a for-loop

headline_dataframe = pd.DataFrame(headline_data).T

# uncomment line belwo to save as json file

#headline_dataframe.to_json('fundref_altmetric.json')