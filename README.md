# FundRef Altmetrics#

A Python script combining data from the CrossRef and Altmetric APIs to find the altmetric scores of research organised by funder.

- CrossRef API: https://github.com/CrossRef/rest-api-doc/blob/master/rest_api.md
- Altmetric API: http://api.altmetric.com/

Takes only the first 100 funders from http://api.crossref.org/funders. Then it fetches the works associated with each funder and then it fetches the Altemtric data for each work. The data is organised in a Pandas DataFrame.  A second dataframe is created with just headline data.

I run this script in ipython which then gives access to the two dataframes.

### Dependencies
* ipython==2.3.1
* pandas==0.15.1
* requests==2.43

